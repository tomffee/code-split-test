const ROUTES = {
  HOME: '/',
  RANDOM: '/random',
  NOT_FOUND: '/not-found'
};

export {
  ROUTES
};