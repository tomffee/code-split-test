/* eslint-disable no-unreachable */
import { combineReducers } from 'redux';

import app from './app/reducer';

const appReducers = combineReducers({
  app
});

const rootReducer = (state, action) => {
  return appReducers(state, action);
};

export { rootReducer };
