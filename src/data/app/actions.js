
// Constants
import * as c from './constants';

const onLocalizationChange = (language) => ({
  type: c.ON_LOCALIZATION_CHANGE,
  payload: {
    language
  }
});

export const onMediaQueryChange = (matches) => ({
  type: c.ON_MQL_CHANGE,
  payload: {
    matches
  }
});

export const changeLocalization = (newLanguage) => (dispatch, getState) => {
  if (getState().app.get('language') === newLanguage) return;

  dispatch(onLocalizationChange(newLanguage));
  if (typeof(Storage) !== "undefined") {
    localStorage.setItem('vyfakturujto:lang', newLanguage);
  }
};

export const changeDefaultLanguageIfNeeded = () => (dispatch, getState) => {
  if (typeof(Storage) !== "undefined") {
    if (localStorage.getItem('vyfakturujto:lang') && localStorage.getItem('vyfakturujto:lang').length !== 0) {
      dispatch(changeLocalization(localStorage.getItem('vyfakturujto:lang')));
    } else {
      localStorage.setItem('vyfakturujto:lang', getState().app.get('language'));
    }
  }
};