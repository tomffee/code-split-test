import { fromJS } from 'immutable';

// Constants
import { CONSTANTS } from './constants';

const initialState = fromJS({
  version: "0.0.1"
});

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CONSTANTS.ON_VERSION:
      return state.set('version', payload.version);

    default:
      return state;
  }
};

export default reducer;
