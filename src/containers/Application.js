import React, { Component } from 'react';
import { connect } from 'react-redux';

// Components
import { BrowserRouter as Router, Route/*, Redirect*/, Switch } from 'react-router-dom';
// --- Routes
import Home from '../components/Home';
import { Random } from '../components/Random';
import { NoMatch } from '../components/NoMatch';

// Constants
import { ROUTES } from "../constants/routes";

// Others

class Application extends Component {
  render() {
    return (
      <Router>
        <div>
          <Switch>
            <Route path={ROUTES.HOME} exact component={Home} />
            <Route path={ROUTES.RANDOM} component={Random} />
            <Route component={NoMatch} />
          </Switch>
        </div>
      </Router>
    );
  }


}

export default connect(
  (state, ownProps) => {
    return ({
      ...ownProps,
    });
  }, {
  }
)(Application);

