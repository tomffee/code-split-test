// React
import React from 'react';
import ReactDOM from 'react-dom';

// Redux
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

// Routes & Reducers
import Application from './Application';
import { rootReducer } from '../data/reducer';

// Styles
import '../assets/styles/main.css';

const initialState = {};

// Step 1. Setup store and middlewares
export const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(
    thunk
  )
);

// Step 2. Get document element by ID
const rootNode = document.getElementById('root');

// Step 3. Render Root component to document root node
ReactDOM.render((
  <Provider store={store}>
    <Application />
  </Provider>
), rootNode);
