import React from 'react';
import { Link } from 'react-router-dom';
import { ROUTES } from '../constants/routes';

const Home = ({
  ...props
}) => {
  return (
    <div>
      This is home. <Link to={ROUTES.RANDOM}>Goto random</Link>
    </div>
  );
};

export default Home;
