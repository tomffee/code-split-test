import React, { Component } from 'react';


class Random extends Component {
  state = {
    Version: null
  };

  handleVersionAsync = (e) => {
    e.preventDefault();

    const version = import('../components/Version')
      .then(module => module.default)
      .then(AsyncComponent => this.setState({ Version: AsyncComponent }));
  };


  render() {
    const { Version } = this.state;

    return (
      <div>
        <p>Hi I'm random version:</p>
        {!Version ? (
          <a href="" onClick={this.handleVersionAsync}>Show version component</a>
        ) : (
          <span><Version /></span>
        )}
      </div>
    );
  }
};

export { Random };
