import React from 'react';

const NoMatch = ({...props}) => {
  return (
    <div>
      <h1>No match</h1>
    </div>
  );
};

export { NoMatch };
