import React from 'react';
import { connect } from 'react-redux';

const Version = ({
  version,
  ...props
}) => {
  return (
    <b>{version}</b>
  );
};

export default connect(
  (state, ownProps) => ({
    ...ownProps,
    version: state.app.get('version')
  })
)(Version);
